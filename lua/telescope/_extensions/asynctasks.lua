local has_telescope, telescope = pcall(require, 'telescope')

if not has_telescope then
  error('This plugins requires github.com/nvim-telescope/telescope.nvim')
end

local pickers = require('telescope.pickers')
local sorters = require('telescope.sorters')
local actions = require('telescope.actions')
local finders = require('telescope.finders')
local asynctasks_table = {}
local asynctasks_names = {}


function run_task(prompt_bufnr)
	local selection = actions.get_selected_entry(prompt_bufnr)
	local task_index  = 0

	for index, data in ipairs(asynctasks_names) do
		if data == selection[1] then
			task_index = index
		end
	end

	actions.close(prompt_bufnr)

	vim.cmd('AsyncTask ' .. asynctasks_table[task_index]['name'])
end


function mapping(prompt_bufnr, map)
	map('i', '<CR>', run_task)
	map('n', '<CR>', run_task)

	return true
end


function select_task()
	pickers.new(opts, {
		prompt_title = 'AsyncTask Launch',
		finder    = finders.new_table {
			results = asynctasks_names
		},
		sorter = sorters.get_generic_fuzzy_sorter(),
		attach_mappings = mapping
	}):find()
end

function list(opts)
	local has_asynctasks, result = pcall(vim.api.nvim_call_function, 'asynctasks#list', {''})

	if not has_asynctasks then
	  error('This plugins requires github.com/skywind3000/asynctasks.vim')
	end

	asynctasks_table = result

	if vim.tbl_isempty(asynctasks_table) then
        error('There was no local or global task found.')
    end

	for index, value in ipairs(asynctasks_table) do
		asynctasks_names[index] = value['name'] .. ', <' .. value['scope'] .. '>, ' .. value['command']
	end

	select_task()
end

return telescope.register_extension{exports = {list = list}}
